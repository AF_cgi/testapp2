//
//  Car.swift
//  FastlaneTests
//
//  Created by Christoph G. on 16.03.18.
//  Copyright © 2018 APPSfactory GmbH. All rights reserved.
//

import Foundation

class Car {
	
	var miles = 0
	var type: CarType
	var transmissionMode: CarTransmissionMode
	
	
	// MARK: - Init
	
	init(type: CarType, transmissionMode: CarTransmissionMode) {
		
		self.type             = type
		self.transmissionMode = transmissionMode
	}
	
	
	// MARK: - Public
	
	func start(minutes: Int) {
		
		switch self.transmissionMode {
		case .drive:
			
			var speed: Int {
				switch self.type {
				case .economy:	return 35
				case .offRoad:	return 50
				case .sport:	return 70
				}
			}
			
			self.miles = speed * (minutes / 60)
			
		case .neutral, .park, .reverse:
			self.miles = 0
		}
	}
}
