//
//  Created by APPSfactory GmbH
//  Copyright © 2017 APPSfactory GmbH. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // MARK: - Life cycle
    
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let car = Car(type: .sport, transmissionMode: .drive)
		car.start(minutes: 120)
		
		print("miles: \(car.miles)")
	}
}
