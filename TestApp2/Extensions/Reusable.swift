//
//  Created by APPSfactory GmbH
//  Copyright © 2017 Appsfactory GmbH. All rights reserved.
//

import UIKit

protocol Reusable {}


// MARK: - UICollectionViewCell

extension UICollectionReusableView: Reusable {}

extension Reusable where Self: UICollectionViewCell {
	
	static var nib: UINib {
		return UINib(nibName: self.reuseIdentifier, bundle: Bundle(for: self))
	}
	static var reuseIdentifier: String {
		return String(describing: self)
	}
}

extension Reusable where Self: UICollectionReusableView {
	
	static var nib: UINib {
		return UINib(nibName: self.reuseIdentifier, bundle: Bundle(for: self))
	}
	static var reuseIdentifier: String {
		return String(describing: self)
	}
}


// MARK: - UICollectionView

extension UICollectionView {
	
	func register<T: UICollectionViewCell>(cellType: T.Type) {
		self.register(T.nib, forCellWithReuseIdentifier: T.reuseIdentifier)
	}
	
	func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath, cellType: T.Type = T.self) -> T {
		
		guard let cell = self.dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
			fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
		}
		
		return cell
	}
	
	func register<T: UICollectionReusableView>(supplementaryViewType: T.Type, ofKind elementKind: String) {
		self.register(T.nib, forSupplementaryViewOfKind: elementKind, withReuseIdentifier: T.reuseIdentifier)
	}
	
	func dequeueReusableSupplementaryView<T: UICollectionReusableView>
		(ofKind elementKind: String, for indexPath: IndexPath, viewType: T.Type = T.self) -> T {
		
		let dequeueView = self.dequeueReusableSupplementaryView(ofKind: elementKind, withReuseIdentifier: T.reuseIdentifier, for: indexPath)
		guard let view = dequeueView as? T else {
			fatalError("Could not dequeue supplementary view with identifier: \(T.reuseIdentifier)")
		}
		
		return view
	}
}


// MARK: - UITableViewCell

extension UITableViewCell: Reusable {}
extension UITableViewHeaderFooterView: Reusable {}

extension Reusable where Self: UITableViewCell {
	
	static var nib: UINib {
		return UINib(nibName: self.reuseIdentifier, bundle: Bundle(for: self))
	}
	static var reuseIdentifier: String {
		return String(describing: self)
	}
}

extension Reusable where Self: UITableViewHeaderFooterView {
	
	static var nib: UINib {
		return UINib(nibName: self.reuseIdentifier, bundle: Bundle(for: self))
	}
	static var reuseIdentifier: String {
		return String(describing: self)
	}
}


// MARK: - UITableView

extension UITableView {
	
	func register<T: UITableViewCell>(cellType: T.Type) {
		self.register(T.nib, forCellReuseIdentifier: T.reuseIdentifier)
	}
	
	func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath, cellType: T.Type = T.self) -> T {
		
		guard let cell = self.dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
			fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
		}
		
		return cell
	}
	
	func register<T: UITableViewHeaderFooterView>(headerFooterViewType: T.Type) {
		self.register(T.nib, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
	}
	
	func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>(_ viewType: T.Type = T.self) -> T? {
		
		guard let view = self.dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier) as? T? else {
			fatalError("Could not dequeue header/footer with identifier: \(T.reuseIdentifier)")
		}
		
		return view
	}
}
