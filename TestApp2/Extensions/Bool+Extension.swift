//
//  Created by APPSfactory GmbH
//  Copyright © 2017 Appsfactory GmbH. All rights reserved.
//

import Foundation

extension Bool {
	
	/// Toogle the state of a boolean value to its opposite
	mutating func toogle() {
		self = !self
	}
}
