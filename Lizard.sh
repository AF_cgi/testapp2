#!/bin/bash

if hash lizard 2>/dev/null; then
	exit 0
else
	echo 'Skipping Lizard (not installed!)'
    exit 1
fi
